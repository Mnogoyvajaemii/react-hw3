import { useState, useEffect } from "react";
import "./productCard.scss";
import { StarIcon, StarFillIcon } from "../icons";
import Modal from "../modal";

function ProductCard(props) {
  const [favorite, setFavorite] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleHideModal = () => {
    setShowModal(false);
  };

  const isFav = props.favItems.includes(props.id);
  const isCart = props.cartItems.includes(props.id);
  return (
    <>
      <li className="product-card">
        {props.removeCard && <div className="card-closedBtn" onClick={handleShowModal}></div>}
        <div
          onClick={() => {
            props.addToFav(props.id);
            setFavorite(!favorite);
          }}
          style={{ cursor: "pointer" }}
        >
          {isFav ? <StarFillIcon /> : <StarIcon />}
        </div>
        <a className="product-card__link" href="">
          <img className="product-card__image" src={props.image} alt="Card paint" />
        </a>
        <a className="product-card__link card__link--name" href="">
          <h2 className="product-card__title">{props.name}</h2>
        </a>
        <p className="product-card__color">
          Колір - <span>{props.color}</span>
        </p>
        <p className="product-card__price">
          Ціна : <span>{props.price}</span>
        </p>
        <p className="product-card__number">
          Код товару : <span>{props.id}</span>
        </p>
        <button className="product-card__btn" onClick={handleShowModal} disabled={isCart ? true : false}>
          Купити
        </button>
      </li>

      {showModal && (
        <Modal
          removeCard={props.removeCard}
          addToCart={props.addToCart}
          cartItems={props.cartItems}
          onClose={handleHideModal}
          src={props.image}
          text={props.name}
          price={props.price}
          id={props.id}
        />
      )}
    </>
  );
}

export default ProductCard;
