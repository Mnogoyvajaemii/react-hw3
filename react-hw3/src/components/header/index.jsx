import { useState, useEffect } from "react";
import "./header.scss";
import { StarIcon, ShoppingCart } from "../icons";
import { NavLink } from "react-router-dom";

const Header = (props) => {
  const [cartItemsState, setCartItemsState] = useState(props.cartItems);
  const [favItemsState, setFavItemsState] = useState(props.favItems);

  useEffect(() => {
    setCartItemsState(props.cartItems);
    setFavItemsState(props.favItems);
  }, [props.cartItems, props.favItems]);

  return (
    <>
      <header className="header-page">
        <div className="container header-page--container">
          <nav className="header-page__navbar">
            <div className="navbar__main-link">
              <NavLink to={"/"}>
                <p className="icon-descr">Головна сторінка</p>
              </NavLink>
            </div>
            <div className="header-page__links">
              <NavLink to={"/cart"}>
                <div className="header-page__icons">
                  <p className="icon-descr">Кошик</p>
                  {cartItemsState.length > 0 ? (
                    <div className="counter">
                      <p>{cartItemsState.length}</p>
                    </div>
                  ) : null}
                  <ShoppingCart className={"header-page__icon"} />
                </div>
              </NavLink>
              <NavLink to={"/favorite"}>
                <div className="header-page__icons">
                  {favItemsState.length > 0 ? (
                    <div className="counter">
                      <p>{favItemsState.length}</p>
                    </div>
                  ) : null}
                  <p className="icon-descr">Обране</p>
                  <StarIcon className={"header-page__icon"} />
                </div>
              </NavLink>
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
