import ProductList from "../productList";
import "./main.scss";

function Main(props) {
  return (
    <>
      <main className="main-page">
        <div className="container container--products">
          <ProductList
            removeCard={props.removeCard}
            addToCart={props.addToCart}
            addToFav={props.addToFav}
            favItems={props.favItems}
            cartItems={props.cartItems}
            cardData={props.cardData}
            dataLoaded={props.dataLoaded}
          />
        </div>
      </main>
    </>
  );
}

export default Main;
