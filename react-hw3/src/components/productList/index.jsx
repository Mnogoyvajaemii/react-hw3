import React from "react";
import ProductCard from "../productCard";

import "./productList.scss";

function ProductList(props) {
  const { cardData, dataLoaded } = props;

  let itemsList;

  if (!dataLoaded) {
    itemsList = <p>Loading...</p>;
  } else if (cardData && cardData.length > 0) {
    itemsList = cardData.map((item) => {
      return (
        <ProductCard
          removeCard={props.removeCard}
          setFav={props.setFav}
          addToCart={props.addToCart}
          addToFav={props.addToFav}
          cartItems={props.cartItems}
          favItems={props.favItems}
          name={item.name}
          price={item.price}
          color={item.color}
          key={item["item number"]}
          id={item["item number"]}
          image={item.image}
        />
      );
    });
  } else {
    itemsList = <p>No items found.</p>;
  }

  return <ul className="product-list">{itemsList}</ul>;
}

export default ProductList;
