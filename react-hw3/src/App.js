import "./App.css";
import { Routes, Route } from "react-router-dom";
import { Home, Favorite, Cart, NotFound } from "./pages";
import { getProductsFromLS } from "./functions/getProductsFromLS";
import { useState } from "react";
import Header from "./components/header";

function App() {
  const [favItems, setFavItems] = useState(getProductsFromLS("favItems"));
  const [cartItems, setCartItems] = useState(getProductsFromLS("cartItems"));

  return (
    <>
      <Header favItems={favItems} cartItems={cartItems} />

      <Routes>
        <Route
          path="/"
          element={<Home favItems={favItems} cartItems={cartItems} setFav={setFavItems} setCart={setCartItems} />}
        />
        <Route
          path="/favorite"
          element={<Favorite favItems={favItems} cartItems={cartItems} setFav={setFavItems} setCart={setCartItems} />}
        />
        <Route
          path="/cart"
          element={<Cart favItems={favItems} cartItems={cartItems} setFav={setFavItems} setCart={setCartItems} />}
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
