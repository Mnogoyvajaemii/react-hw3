import Main from "../components/main";
import React, { useState, useEffect } from "react";

export function Home(props) {
  const [cardData, setCardData] = useState(null);
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => {
        setCardData(data);
        setDataLoaded(true);
      })
      .catch((error) => console.log(error));
  }, []);

  const addToFav = (id) => {
    const arr = [...props.favItems];
    if (!arr.includes(id)) {
      arr.push(id);
    } else {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }
    localStorage.setItem("favItems", JSON.stringify(arr));
    props.setFav(arr);
  };

  const addToCart = (id) => {
    const arr = [...props.cartItems];
    if (!arr.includes(id)) {
      arr.push(id);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    props.setCart(arr);
  };
  return (
    <>
      <Main
        setFav={props.setFav}
        addToCart={addToCart}
        addToFav={addToFav}
        favItems={props.favItems}
        cartItems={props.cartItems}
        cardData={cardData}
        dataLoaded={dataLoaded}
      />
    </>
  );
}
