import Main from "../components/main";
import React, { useState, useEffect } from "react";

export function Favorite({ favItems, cartItems, setFav, setCart }) {
  const [cardData, setCardData] = useState(null);
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => {
        const newData = favItems.map((favItem) => data.find((cardItem) => cardItem["item number"] === favItem));
        setCardData(newData);
        setDataLoaded(true);
      })
      .catch((error) => console.log(error));
  }, [favItems]);

  const addToFav = (id) => {
    const arr = [...favItems];
    if (!arr.includes(id)) {
      arr.push(id);
    } else {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }
    localStorage.setItem("favItems", JSON.stringify(arr));
    setFav(arr);
  };

  const addToCart = (id) => {
    const arr = [...cartItems];
    if (!arr.includes(id)) {
      arr.push(id);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    setCart(arr);
  };

  return (
    <>
      <div className="container">
        <h1 style={{ marginBottom: 0, fontWeight: 500, color: "rgb(0,60,128)" }}>Обране</h1>
      </div>
      <Main
        setFav={setFav}
        addToCart={addToCart}
        addToFav={addToFav}
        favItems={favItems}
        cartItems={cartItems}
        cardData={cardData}
        dataLoaded={dataLoaded}
      />
    </>
  );
}
