import Main from "../components/main";
import React, { useState, useEffect } from "react";

export function Cart({ favItems, cartItems, setFav, setCart }) {
  const [cardData, setCardData] = useState(null);
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => {
        const newData = cartItems.map((cartItem) => data.find((cardItem) => cardItem["item number"] === cartItem));
        setCardData(newData);
        setDataLoaded(true);
      })
      .catch((error) => console.log(error));
  }, [cartItems]);

  const addToFav = (id) => {
    const arr = [...favItems];
    if (!arr.includes(id)) {
      arr.push(id);
    } else {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }
    localStorage.setItem("favItems", JSON.stringify(arr));
    setFav(arr);
  };

  const addToCart = (id) => {
    const arr = [...cartItems];
    if (!arr.includes(id)) {
      arr.push(id);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    setCart(arr);
  };

  const removeFromCart = (id) => {
    const arr = [...cartItems];
    if (arr.includes(id)) {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    setCart(arr);
  };

  return (
    <>
      <div className="container">
        <h1 style={{ marginBottom: 0, fontWeight: 500, color: "rgb(0,60,128)" }}>Кошик</h1>
      </div>
      <Main
        removeCard={removeFromCart}
        setFav={setFav}
        addToCart={addToCart}
        addToFav={addToFav}
        favItems={favItems}
        cartItems={cartItems}
        cardData={cardData}
        dataLoaded={dataLoaded}
      />
    </>
  );
}
